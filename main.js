import lsystem from './l-system.js';
import rules from './rules.js';
import {GUI} from "./dat.gui.module.min.js";
import initGUI from './gui.js';

const {abs, min, max, floor, round, random} = Math;
const canvas = document.querySelector('canvas');
const ctx = canvas.getContext('2d');
const ww = 600, wh = 600;
var model, idx = 0, seed = 5, n = 4, lev = 1, amp = 1, rbutton;

const gui = {
    name: null,
    open: true,
    fields: [
        {
            idx: [idx, 0, rules.length-1, 1],
            onChange: v => {
                idx = v;
                if(idx > 10) 
                    rbutton.visibility = 'visible';
                else rbutton.visibility = 'hidden';
                model = lsystem(rules[idx], rules[idx].n, 1, seed);
                display(ctx, model, lev, amp);
            }
        },
        {
            n : [n, 2, 7, 1],
            onChange: v =>{
                model = lsystem(rules[idx], (n = v), 1, seed);
                display(ctx, model, lev, amp);
            }
        },
        {
            len: [lev, 0, 1, .001],
            onChange: v =>{
                display(ctx, model, (lev = v), amp);                
            }
        },
        {
            amp: [amp, .2, 1.8, .001],
            onChange: v =>{
                display(ctx, model, lev, (amp = v));
            }
        },
        {
            seed: ()=>{ 
                seed = random()*999;
                model = lsystem(rules[idx], n || rules[idx].n, 1, seed);
                display(ctx, model, lev, amp);
            }
        }
    ]
};

initGUI(new GUI(), gui);
rbutton = gui._gui.__controllers[4].__li.style;
rbutton.visibility = 'hidden';
model = lsystem(rules[idx], rules[idx].n, 1, seed);
display(ctx, model, lev, amp);

function display(ctx, model, f=1, amp=1){
    ctx.clearRect(0, 0, ww, wh);
    const n = max(floor(model.i.length*f),1);
    for(let i = 0; i < n; i++){     
        let a = model.v[model.i[i][0]]; 
        let b = model.v[model.i[i][1]];
        line(ctx, ww*.7, wh*.7, a[0], a[1], b[0], b[1], amp);      
    }
}

function line(ctx, w, h, ax, ay, bx, by, a=1){
    ctx.beginPath();
    ctx.moveTo(a*ax*w*.5 +w*.5, a*ay*h*.5+h*.5);
    ctx.lineTo(a*bx*w*.5 +w*.5, a*by*h*.5+h*.5);
    ctx.closePath();
    ctx.stroke();
}