const arr = [
{
	axiom: 'X',
	theta: 24,
	delta: 5,
	n: 5,
	F:'FF',
	X: 'F-[[X]+X]+F[+FX]-X'
},
{	
	axiom: 'F+F+F+F',
	theta: 90,
	delta: 6,
	n: 2,
	F:'F+F-F-FF+F+F-F'
},
{
  axiom: '[X]++[X]++[X]++[X]++[X]',
  theta: 36,
  delta: 20,
  n: 4,  
  W: 'YF++ZF----XF[-YF----WF]++',
  X: '+YF--ZF[---WF--XF]+',
  Y: '-WF++XF[+++YF++ZF]-',
  Z: '--YF++++WF[+ZF++++XF]--XF',
  F: '' 
},
{
    axiom: 'F',
    theta: 60, //90
    delta:5,
    n: 3,
    F: '+F+F-F-F+F+F+F-F-F+F'

},
{
	axiom: 'F',
	theta: 28,
	delta: 10,
	n: 3,
	F: 'FF+[+F-F-F]-[-F+F+F]'
},
{
	axiom: 'VZFFF',
	theta: 20,
	delta: 8,
	n: 9,
	V: '[+++W][---W]YV',
	W: '+X[-W]Z',
	X: '-W[+X]Z',
	Y: 'YZ',
	Z: '[-FFF][+FFF]F'
},
{
	axiom: 'F',
	theta: 60,
	delta: 5,
	n: 5,
	F: 'FF-[XY]+[XY]',
	X: '+FY',
	Y: '-FX'
},
{
	axiom: 'X',
	theta: 90,
	delta: 18,
	n: 3,
	X:'^\\XF^\\XFX-F^//XFX&F+//XFX-F/X-/'
},
{
	axiom: 'F',
	theta: 30,
	delta: 5,
	n: 5,
	F: 'FF-[FY]+[XY]',
	X: '+F--Y',
	Y: 'Y-Y'
},
{
	axiom: 'F',
	theta: 40,
	delta: 5,
	n: 5,
	F: 'FF-[XY]+[XY]',
	X: '+F+--X+Y',
	Y: '-FX'
},
{
	axiom: 'F',
	theta: 60,
	delta: 4,
	n: 5,
	F: ['FF-[XY]+[XY]', 'FYF-[XY]+[--XY]'],
	X: '+F+--X+Y',
	Y: '-FX'
},
// stochastic rules
{ 
    theta: 45,
    delta: 10,
    n: 3,
    axiom: '+WF--YF--ZF',
    W: 'YF++ZF----XF[-YF----WF]++',
    X: '+YF--ZF[---WF--XF]+',
    Y: ['-WF++XF[+++YF++ZF]-', '+XF[YFZF]-', 'XX-F'],
    Z: '--YF++++WF[+ZF++++XF]--XF',
    F: 'FF'
},
{ 
    theta: 45,
    delta: 6,
    n: 3,
    axiom: 'F',
    F: ['F[+FF]F[-FF]F', 'FF[+FF]F[-FFF]', 'F[+FFF]Z[-FF]F', 'F[+FFF]Z[-FF]F'],
    Z: ['', '-', 'F[ZF]-']
},
{ 
    theta: 20,
    delta: 12,
    n: 3,
    axiom: 'FXFXFX',
    F: '',
    X: ['[FX-FY][-FX-FY-FX][ZZ]-FY-FX+FY+FX', '[FX-FY]-FF+FY+FX'],
    Y: 'FY',
    Z: '-FX-FY-FX'
},
{ 
    axiom: 'X',
    theta: 24,
    delta: 5,
    n: 4,
    F:['FF', 'FF', 'XF', '+FXF'],
    X: ['F-[[X]+X]+F[+FX]-X', 'F-[[-X]+X]+[F[+FX]-]FXX', 'F-[-F[-X]F+X]+F[+FX]-X']
},
{   
    axiom: 'F',
    theta: 60,
    delta: 5,
    n: 5,
    F: ['FF-[XY]+[XY]', 'FF-[YY]+[Y-XY]'],
    X: ['+FY', 'F+XY', '-F+FXY'],
    Y: ['-X', '-XF-X']
},
{  
    axiom: 'F',
    theta: 30,
    delta: 5,
    n: 5,
    F: ['FF-[FY]+[XY]', 'FF-[FF+Y]+[XY]', 'F-[FY]+[F+XY]'],
    X: ['+FX--Y', 'X-Y', 'X[F]-Y--F'],
    Y: ['XX','-XYX', '-Y-FX']
},
{  
    axiom: 'X+F',
    theta: 45,
    delta: 6,
    n: 4,
    F: ['+X-F', '-XX+F', '+XF+'],
    X:['-FFXF[X+FF-F+]', '-FF[X+XX]-F', 'FXF[X+[F-]XF+]']
},
{  
    axiom: 'F+F+F+F',
    theta: 90,
    delta: 8,
    n: 2,
    F:['F+F-F-FF+F+F-F', 'F--FF-F-FF+F+F-F', 'F--FF-F-F+F+F+F-F', 'F+F-FF+F-FF+F+F']
},
{
    theta: 60,
    delta: 10,
    n: 3,
    axiom:'+WF--XF---YF--ZF',
    F: 'FF',
    W: ['YF++ZF----XF[+++]++', 'YF++ZF----XF[-YF----WF]++'],
    X: '+YF--ZF[---WF--XF]+',
    Y: ['-WF++XF[+++YF++ZF]-', '-XY[F++F]'],
    Z: '--YF++++WF[+ZF++++XF]--XF'
}
];

export default arr;